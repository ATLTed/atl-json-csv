# atl-jsonCSV

Simple library to convert objects to csv/tsv and back.


## Install

```shell
npm install git+https://bitbucket.org/ATLTed/atl-json-csv.git#master
```

## Example Usage

```javascript
const jsonCSV = require('atl-json-csv');

let exampleArray = [{a:"a1",b:"b1",c:"b3"}, {a:"a1",b:"b1",c:"b3"};]
let csv = jsonCSV.jsonCSV(exampleArray)
console.log(csv);

await jsonCSV.writeFile('/tmp/data.csv', csv);
```
const fs = require('fs')

exports.tsvJSON = function(csv){
    return exports.csvJSON(csv, "\t")
}

exports.csvJSON = function(csv, delim){
    delim = delim === undefined ? "," : delim;
    var lines=csv.split("\n");
    var result = [];
    var headers=lines[0].replace(/\"/g, "").split(delim);
    
    for(var i=1;i<lines.length;i++){
        var obj = {};
        var currentline=lines[i].replace(/\"/g, "").split(delim);
        
        for(var j=0;j<headers.length;j++){
            obj[headers[j]] = currentline[j];
        }
        result.push(obj);
    }
    return result; //JavaScript object
}

exports.jsonTSV = function(csv, willGoDeep){
    return exports.jsonCSV(csv, "\t", willGoDeep)
}

exports.jsonCSV = function(array, delim, willGoDeep) {
    delim = delim === undefined ? "," : delim;
    willGoDeep = willGoDeep === undefined ? true : willGoDeep;
    if (array.length === undefined){
        array = Object.keys(array).map(function (key) {return array[key]});
    }
    var keys = [];
    for (var k in array[0]) keys.push(k);

    // Build header
    var result = keys.join(delim) + "\n";

    // Add the rows
    array.forEach(function(obj){
        keys.forEach(function(k, ix){
            if (ix) result += delim;
            var value = !willGoDeep && typeof obj[k] === "object" ? "object" : obj[k]
            result += value;
        });
        result += "\n";
    });

    return result;
}

exports.expandSlimJSON = function(data){
    var result = [];
    result = data.data.values.map(function(value_set){
        var obj = {}; 
        for(i = 0; i < data.data.fields.length; i++ )
            obj[data.data.fields[i]]=value_set[i];      
        return obj;
    })
    return result;
}

exports.slimJSON = function(data, keys, willReturnRetcode){
	willReturnRetcode = willReturnRetcode === undefined ? false : willReturnRetcode;

	var result = {fields:[], values:[]}

	if( data instanceof Array ) {
		if (data.length > 0){
			if (keys === undefined) {
				keys = Object.keys(data[0]);
			}
			result.fields = keys;


			data.forEach(function(row) {
				var values = [];
				keys.forEach(function(key) {
					values.push(row[key]);
				});
				result.values.push(values);
			});
		}

		if (willReturnRetcode){
			result = {"retcode":0, "data":result};
		}
	} else {
		result = []
		keys = Object.keys(data);
		keys.forEach(function(key) {
			result.push(data[key]);
		});
	}
	return result;

	
}

exports.readFile = function(path, opts = 'utf8'){
  new Promise((res, rej) => {
      fs.readFile(path, opts, (err, data) => {
          if (err) rej(err)
          else res(data)
      })
  })
}

exports.writeFile =function(path, data, opts = 'utf8'){
  new Promise((res, rej) => {
      fs.writeFile(path, data, opts, (err) => {
          if (err) rej(err)
          else res()
      })
  })
}